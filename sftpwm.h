/*
 * pwm.h
 * a collection of functions for implementing custom
 * in-software pwm resolutions for your program.
 *
 * Created: 2/29/2020 1:22:39 AM
 * Last modified: March 2 2020
 * Author: UniX
 */

#ifndef SFTPWM_H_
#define SFTPWM_H_

    //dependencies:
    #include <stdint.h>
    #include "singly-linked-list/linkedlist.h"

    //parameters and tunables:
    /*
        frequency at which you call 'varfreqpwm_isr'
        which is meant to be called from your ISR handler:
    */
    #define SFTPWM_ISR_FREQ 1000

    /*
        frequency of the software timer is equal to the required
        pwm frequency multiplied by the required pwm resolution.
    */
    #define SFTTIMER_FREQ (pwm_frequency*pwm_resolution)

    /*
        the software timer increments every DIVIDER
        times xxxpwm_isr function is called.
    */
    #define DIVIDER (SFTPWM_ISR_FREQ/SFTTIMER_FREQ)
    //#define DIVIDER_VAR (temp_divider_ptr->next->val)
    /*
        divider values 0 through 84 are stored temporarily
        (temp_divider) in nodes 85 through 169, timer-counter
        values are stored in nodes 170 through 254.
    */

    #define COMPARE_INDEX (pwm_index+64)
    #define RESOLUTION_INDEX (pwm_index+128)
    #define TIMER_COUNTER_INDEX (pwm_index+192)

    #define COMPARE divider->next
    #define RESOLUTION divider->next->next

    #define TEMP_COMPARE temp_divider->next
    #define TEMP_RESOLUTION temp_divider->next->next

    //variable declarations:
    extern strctype_listnode* varfreqpwm_set_listptr;
    extern strctype_listnode* varfreqpwm_isr_listptr;

    /*
        some notes on the following functions:

        .each of these functions are capable of handling and keeping
            track of many pwm signals simultaneously, and different
            instances of these signals are identified via the 'pwm_index'
            argument which is passed to the function.

        .'xxxpwm_set' functions must be used to change the parameters of
            your pwm signal if you wish to do so, and 'xxxpwm_isr' function
            must be called from your ISR handler with the same frequency as
            specified with 'SFTPWM_ISR_FREQ' parameter.

        .these functions use linked list data structures provided by the
            library 'singly-linked-list' to hold the variables required
            for their function; and are most-suitably/typically-should-be
            used for generating low frequency pulses. maximum recommended
            pwm frequency is 1000Hz.

        .each function holds it's data in one singly-linked list, and the
            pointer to the head (first node) on that list is stored in
            'xxxpwm_global_listptr'. this variable then can be used in the
            main program to check if the corresponding function is active
            in the background, in which case it's value could be anything
            but NULL (if xxxpwm_global_listptr is NULL then the function
            is not running in the background.
    */

    //function prototypes:
    /*
        varfreqpwm: generates a pwm with variable frequency and pulse width.
        with this function you can change the parameters related to the generated pwm.
        'pwm_index' is the identifier of your pwm signal, 'pwm_frequency' is pwm frequency,
        'pwm_resolution' is the number of steps of the pwm signal on-time, the pwm signal
        turns on (goes 0) at BOTTOM, and 'compare' is the step number at which the signal goes 1.
        notice that pwm_frequency multiplied by pwm_resolution must not be higher
        than SFTPWM_ISR_FREQ.
        0 >= pwm_index <= 50.
    */
    void varfreqpwm_set(uint8_t pwm_index,int pwm_frequency, int pwm_resolution, int compare);

    /*
        this function must be called from your ISR handler.
        increments the varfreqpwm timer value and returns
        the status of your pulse.
        pwm_index >= 1.
    */
    _Bool varfreqpwm_isr(uint8_t pwm_index);

    /*
        check to see if a certain pwm stream is active.
    */
    _Bool varfreqpwm_isactive(uint8_t pwm_index);

    /*
        stops the running pwm.
    */
    void varfreqpwm_stop(uint8_t pwm_index);

    /*
        stops the running pwm and removes the stored
        settings associated with that pwm signal.
    */
    void varfreqpwm_clear(uint8_t pwm_index);

#endif /* SFTPWM_H_ */