/*
 * pwm.c
 * a collection of functions for implementing custom
 * in-software pwm resolutions for your program.
 *
 * Created: 2/29/2020 1:22:39 AM
 * Last modified: March 2 2020
 * Author: UniX
 */

#include "singly-linked-list/linkedlist.h"
#include "sftpwm.h"

//variable definitions:
strctype_listnode* varfreqpwm_listptr = NULL;

//function definitions:
/*
    varfreqpwm creates a pulse with variable frequency and width, by means of incrementing
    a software timer value to a specific TOP (which is the same as 'pwm_resolution') at
    varrying speeds (to be able to change the pwm frequency without changing the duty cycle)
    by incrementing the software timer value at different rates (set by 'divider').
*/
void varfreqpwm_set(uint8_t pwm_index, int pwm_frequency, int pwm_resolution, int compare)
{
    /*
        check to see if pwm_frequency * pwm_resolution (SFTTIMER_FREQ)
        is less than ISR frequency (if it isn't, generating a pwm
        signal with those requirements wouldn't be possible.)
    */
    if(SFTTIMER_FREQ <= SFTPWM_ISR_FREQ)
    {
        /*
            these next lines of code makes sure 'divider' node
            is right after 'divider-temp' node in the list.
        */
        list_supergrow(&varfreqpwm_set_listptr, RESOLUTION_INDEX, pwm_resolution);
        list_supergrow(&varfreqpwm_set_listptr, COMPARE_INDEX, compare);
        list_supergrow(&varfreqpwm_set_listptr, pwm_index, DIVIDER);
    }
}

_Bool varfreqpwm_isr(uint8_t pwm_index)
{
    strctype_listnode* divider = list_fetch(varfreqpwm_set_listptr, pwm_index);
    strctype_listnode* temp_divider = list_fetch(varfreqpwm_isr_listptr, pwm_index);
    strctype_listnode* timer_counter = NULL;
    _Bool pulse_state = 0;

    if(divider != NULL) //checks to see if the settings for this pwm are initialized.
    {
        if(temp_divider == NULL || (temp_divider->val) == 0) //if the temporary copies of these settings don't exist, makes them:
        {
            list_supergrow(&varfreqpwm_isr_listptr, RESOLUTION_INDEX, RESOLUTION->val);
            list_supergrow(&varfreqpwm_isr_listptr, COMPARE_INDEX, COMPARE->val);
            temp_divider = list_supergrow(&varfreqpwm_isr_listptr, pwm_index, divider->val);
            timer_counter = list_supergrow(&varfreqpwm_isr_listptr, TIMER_COUNTER_INDEX, 0);
        }

        if(listflag_outofmemory == 0 && --(temp_divider->val) == 0)
        {
            temp_divider->val = divider->val;
            timer_counter->val++;
            if(timer_counter->val == TEMP_COMPARE->val)
                pulse_state = 0;
            if(timer_counter->val >= TEMP_RESOLUTION->val)
                timer_counter->val = 0;
                pulse_state = 1;
        }
    }
    return pulse_state;
}

_Bool varfreqpwm_isactive(uint8_t pwm_index)
{
    _Bool isactive;
    strctype_listnode* temp_divider = list_fetch(varfreqpwm_isr_listptr, pwm_index);

    if(temp_divider == NULL)
        isactive = 0;
    else
        isactive = 1;
    return isactive;
}

void varfreqpwm_stop(uint8_t pwm_index)
{
    list_shrink_global(&varfreqpwm_isr_listptr, RESOLUTION_INDEX);
    list_shrink_global(&varfreqpwm_isr_listptr, COMPARE_INDEX);
    list_shrink_global(&varfreqpwm_isr_listptr, pwm_index);
    list_shrink_global(&varfreqpwm_isr_listptr, TIMER_COUNTER_INDEX);
}

void varfreqpwm_clear(uint8_t pwm_index)
{
    varfreqpwm_stop(pwm_index);
    list_shrink_global(&varfreqpwm_set_listptr, RESOLUTION_INDEX);
    list_shrink_global(&varfreqpwm_set_listptr, COMPARE_INDEX);
    list_shrink_global(&varfreqpwm_set_listptr,  pwm_index);
}